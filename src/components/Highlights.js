import React from 'react'

/*react-boostrap components*/
import {
	Row,
	Col,
	Card,
} from 'react-bootstrap'

export default function Highlights(){

	return(

		<Row>
			<Col xs={6} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Learn from Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={6} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Study Now, Pay Later</Card.Title>
						<Card.Text>
					      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={6} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Be Part of the Community</Card.Title>
						<Card.Text>
					      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}