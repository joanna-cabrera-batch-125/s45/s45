import React from 'react';

/*react-boostrap components*/
import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function CourseCard(){

	return(

		<Row className="justify-content-center my-5">
			<Col xs={10} md={6}>
				<Card>
					<Card.Body>
						<Card.Title>Sample Course</Card.Title>
						<Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
						    <Card.Text>
						      This is a sample course offering.
						    </Card.Text>
						<Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
						  <Card.Text>
						    PHP 40,000
						  </Card.Text>
						
				    	<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}

