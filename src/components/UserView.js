import React, {useState, useEffect} from 'react';

import {Container} from 'react-bootstrap';

import Course from "./Course";

export default function UserView({courseData}){

	// console.log(courseData)

	const [courses,setCourses] = useState([])


	useEffect( ()=> {

		const courseArr = courseData.map ( (course) => {
			// console.log(course)

			if (course.isActive === true){
				console.log(course);
				return <Course key={course._id} courseProp={course} />
			} else {
				return null
			}
		})

		setCourses(courseArr)
	}, [courseData])

	return(
		<Container>
			{/*display the courses*/}
			{courses}
		</Container>
	)
}